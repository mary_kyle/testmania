package com.citrix.testing.utils;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

import java.rmi.UnexpectedException;
import java.util.regex.Pattern;


public class NativeUtilsApi {
    private static Api api;

    private interface  Api extends Library {
        Pointer allocateImageFromFile(String path);
        boolean writeImageToFile(String path, Pointer image);
        Pointer allocateImageFromScreen();
        void releaseImage(Pointer image);
        int matchTemplateAgainstImage(Pointer template, Pointer image, int matchMethod, double[] threshold, int[] rectangle);

        boolean moveMouseTo(int x, int y, int steps, int totalMillis);
        boolean clickMouseButton(int button);
        boolean writeChar(int c);
        boolean pressVirtualKey(int virtualKey);

        boolean killProcesses(int[] processes, int procCount);
        boolean listProcesses(String pathPattern, int[] processes, int[] procCount);

        int deleteRegKeysByPattern(long hkey, String basePath, String keyNamePattern);
        //boolean deleteRegValuesByPattern(int hkey, String keyPath, String valueNamePattern);

        int uninstallProductsByPattern(String productCodeFilter, String productNamePattern, String publisherPattern);

        String getKnownFolderPath(int folderId);
    }

    private static void initialize() {
        if (api == null) {
            if (Pointer.SIZE == 4) {
                api = (Api) Native.loadLibrary("com.citrix.testing.utils.native32", Api.class);
            } else {
                api = (Api) Native.loadLibrary("com.citrix.testing.utils.native64", Api.class);
            }
        }
    }

    public NativeUtilsApi() {
        initialize();
    }

    public boolean moveMouseTo(int x, int y, int steps, int totalMillis) {
        return api.moveMouseTo(x, y, steps, totalMillis);
    }

    public boolean clickMouseButton(int button) {
        return api.clickMouseButton(button);
    }

    public boolean writeChar(int c) {
        return api.writeChar(c);
    }

    public boolean pressVirtualKey(int virtualKey) {
        return api.pressVirtualKey(virtualKey);
    }


    public Pointer allocateImageFromScreen() {
        return api.allocateImageFromScreen();
    }

    public boolean writeImageToFile(String path, Pointer image) {
        return api.writeImageToFile(path, image);
    }

    public Pointer allocateImageFromFile(String path) {
        return api.allocateImageFromFile(path);
    }

    public void releaseImage(Pointer image) {
        api.releaseImage(image);
    }

    public boolean killProcesses(int[] processes) {
        return api.killProcesses(processes, processes.length);
    }

    public boolean listProcesses(String pathPattern, int[] processes, int[] procCount) {
        if ((procCount != null) && (procCount.length < 1)) {
            throw new IllegalArgumentException("Buffer needs to hold at least one integer.");
        }

        if ((processes != null) && (procCount != null) && (processes.length < procCount[0])) {
            throw new IllegalArgumentException("Buffer is smaller than indicated.");
        }

        return api.listProcesses(pathPattern, processes, procCount);
    }

    public int matchTemplateAgainstImage(Pointer template, Pointer image, int matchMethod, double[] threshold, int[] rectangle) {
        return api.matchTemplateAgainstImage(template, image, matchMethod, threshold, rectangle);
    }

    public boolean deleteRegKeysByPattern(HKEY hkey, String basePath, String keyNamePattern) throws UnexpectedException {
        // give error details, since native API returns just false on invalid pattern syntax...
        Pattern.compile(keyNamePattern);

        long raw;
        switch(hkey) {
            case CLASSES_ROOT: raw = 0x80000000; break;
            case CURRENT_USER: raw = 0x80000001; break;
            case LOCAL_MACHINE: raw = 0x80000002; break;
            default:
                throw new IllegalArgumentException("Unsupported HKEY.");
        }

        int res = api.deleteRegKeysByPattern(raw, basePath, keyNamePattern);

        if(res == -3) {
            throw new IllegalArgumentException("The function was called in an unsupported way.");
        }

        if(res == -1) {
            return false;
        }

        if(res < 0) {
            throw new UnexpectedException("Something went wrong in native code.");
        }

        return res >= 0;
    }

    boolean uninstallProductsByPattern(String productCodeFilter, String productNamePattern, String publisherPattern) throws UnexpectedException {
        // give error details, since native API returns just false on invalid pattern syntax...
        if(productNamePattern != null) {
            Pattern.compile(productNamePattern);
        }

        if(publisherPattern != null) {
            Pattern.compile(publisherPattern);
        }

        int res = api.uninstallProductsByPattern(productCodeFilter, productNamePattern, publisherPattern);

        if(res == -3) {
            throw new IllegalArgumentException("The function was called in an unsupported way.");
        }

        if(res == -1) {
            return false;
        }

        if(res < 0) {
            throw new UnexpectedException("Something went wrong in native code.");
        }

        return res >= 0;
    }

    public String getKnownFolderPath(KnownFolder folder) {
        int raw = 0;
        switch(folder) {
            case Downloads: raw = 0; break;
            case Temp: return System.getProperty("java.io.tmpdir");
            default:
                throw new IllegalArgumentException("Unknown folder.");
        }

        return api.getKnownFolderPath(raw);
    }
}
