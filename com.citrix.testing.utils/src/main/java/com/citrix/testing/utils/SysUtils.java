package com.citrix.testing.utils;

import com.citrix.testing.utils.NativeUtilsApi;
import com.sun.jna.platform.win32.*;

import java.io.File;
import java.io.IOException;
import java.rmi.UnexpectedException;
import java.util.regex.Pattern;


/**
 * Created by natalie on 2/14/2015.
 */
public class SysUtils {
    private static NativeUtilsApi api = new NativeUtilsApi();

    public static boolean killProcessesById(int[] procIds) {
        return api.killProcesses(procIds);
    }

    public static int[] listProcessIdsByPattern(String regexFileNamePattern) {
        // give error details, since native API returns just false on invalid pattern syntax...
        Pattern.compile(regexFileNamePattern);

        int[] processes = new int[1000];
        int[] procCount = new int[] {processes.length};

        if(!api.listProcesses(regexFileNamePattern, processes, procCount)) {
            throw new IllegalArgumentException("Unknown error, most likely argument related.");
        }

        int[] result = new int[Math.min(processes.length, procCount[0])];
        for(int i = 0; i < result.length; i++) result[i] = processes[i];

        return result;
    }

    public static void deleteRegKeysByPattern(HKEY hkey, String basePath, String keyNamePattern) throws IOException {
        if(!api.deleteRegKeysByPattern(hkey, basePath, keyNamePattern)) {
            throw new IOException("Not all matched keys could be deleted.");
        }
    }

    public static void uninstallProductsByPattern(String productCodeFilter, String productNamePattern, String publisherPattern) throws IOException {
        if(!api.uninstallProductsByPattern(productCodeFilter, productNamePattern, publisherPattern)) {
            throw new IOException("Not all matched products could be uninstalled.");
        }
    }

    public static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            Thread.interrupted();
        }
    }

    public static void runUrl(Browsers browser, String url) {

        String browserExe;

        switch(browser) {
            case Firefox: browserExe = "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"; break;
            case IE: browserExe = "C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe"; break;
            case Chrome: browserExe = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"; break;
            default:
                throw new IllegalArgumentException("Unsupported browser.");
        }

        Shell32.INSTANCE.ShellExecute(
                new WinDef.HWND(),
                null,
                browserExe,
                ((browser == Browsers.Chrome) ? "--incognito " : "") + url,
                null,
                WinUser.SW_SHOWMAXIMIZED);
    }

    public static String userDownloadsFolder() {
        return api.getKnownFolderPath(KnownFolder.Downloads);
    }

    public static String userAppDataFolder() {
        return Shell32Util.getSpecialFolderPath(ShlObj.CSIDL_LOCAL_APPDATA, false);
    }

    /**
     * Attempts to delete everything rooted at the given file system node, excluding
     * the node itself. If some child nodes can not be deleted, still all other nodes
     * that can be deleted, will be. In case at least one node could not be deleted,
     * the function returns false, true otherwise.
     */
    public static boolean tryCleanFSTree(String path) {
        try {
            cleanFSTree(path);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static void cleanFSTree(String path) throws IOException {
        File pathNode = new File(path);

        if (pathNode.isDirectory()) {
            org.apache.commons.io.FileUtils.cleanDirectory(pathNode);
        }
    }

    /**
     * Attempts to delete everything rooted at the given file system node, including
     * the node itself. If some child nodes can not be deleted, still all other nodes
     * that can be deleted, will be. In case at least one node could not be deleted,
     * the function returns false, true otherwise.
     */
    public static boolean tryDeleteFSTree(String path) {
        try {
            deleteFSTree(path);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static void deleteFSTree(String path) throws IOException {
        File pathNode = new File(path);

        if (pathNode.isFile()) {
            org.apache.commons.io.FileUtils.forceDelete(pathNode);
        } else if (pathNode.isDirectory()) {
            org.apache.commons.io.FileUtils.deleteDirectory(pathNode);
        }
    }

    public static String tempFolder() {
        return api.getKnownFolderPath(KnownFolder.Temp);
    }

}
