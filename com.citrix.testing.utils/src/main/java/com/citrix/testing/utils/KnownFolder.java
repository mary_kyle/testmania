package com.citrix.testing.utils;

/**
 * Created by natalie on 2/15/2015.
 */
public enum KnownFolder {
    Downloads,
    Temp,
}
