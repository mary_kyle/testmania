package com.citrix.testing.utils.interaction;

import com.citrix.testing.utils.NativeUtilsApi;

import java.awt.*;

/**
 * Created by natalie on 2/14/2015.
 */
public class VirtualInput {
    private NativeUtilsApi api = new NativeUtilsApi();

    public void moveMouseTo(Point xy) {
        api.moveMouseTo(xy.x, xy.y, 30, 500);
    }

    public void moveMouseTo(int x, int y) {
        api.moveMouseTo(x, y, 30, 500);
    }

    public void moveMouseTo(double x, double y) {
        api.moveMouseTo((int)x, (int)y, 30, 500);
    }

    public void leftClick() {
        api.clickMouseButton(0);
    }

    public void write(String text) {
        for(int i = 0; i < text.length(); i++) {
            api.writeChar(text.charAt(i));
        }
    }

    public void press(VirtualKey key) {
        int code = 0;

        switch(key) {
            case ENTER: code = 0x0D; break;
            case TAB: code = 0x09; break;
        }

        if(code == -1)
            throw new IllegalArgumentException("The given key is not supported!");

        api.pressVirtualKey(code);
    }
}
