package com.citrix.testing.utils.interaction;


import java.awt.Point;

public class ImageRegion implements Comparable<ImageRegion> {
    private int left, right, top, bottom;
    private double threshold;
    private String patternName;

    public ImageRegion(String patternName, int left, int right, int top, int bottom, double threshold) {
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
        this.threshold = threshold;
        this.patternName = patternName;
    }

    public double threshold() {
        return threshold;
    }

    public String patternName() {
        return patternName;
    }

    public int left() {
        return left;
    }

    public int right() {
        return right;
    }

    public int top() {
        return top;
    }

    public int bottom() {
        return bottom;
    }

    public int width() {
        return right - left;
    }

    public int height() {
        return bottom - top;
    }

    public Point center() {
        return new Point((right + left) / 2, (bottom + top) / 2);
    }

    @Override
    public int compareTo(ImageRegion o) {
        return Double.compare(threshold, o.threshold);
    }

    @Override
    public String toString() {
        return "{X:" + left + ", Y:" + top + ", Width:" + width() + ", Height:" + height() + ", Threshold:" + threshold() + "}";
    }
}
