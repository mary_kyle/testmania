package com.citrix.testing.utils.interaction;


import com.citrix.testing.utils.NativeUtilsApi;
import com.sun.jna.Pointer;

import java.io.File;
import java.io.IOException;

public class ImagePattern {
    private NativeUtilsApi api = new NativeUtilsApi();
    private Pointer handle;

    private ImagePattern() {
        handle = Pointer.NULL;
    }

    public static ImagePattern fromFile(String pathToImage) throws IOException {
        ImagePattern res = new ImagePattern();

        pathToImage = new File(pathToImage).getCanonicalPath();
        res.handle = res.api.allocateImageFromFile(pathToImage);

        if(res.handle == Pointer.NULL) {
            throw new IOException("Image could not be loaded from '" + pathToImage + "'. The file either doesn't exist, or the format is unsupported (PNG, TIFF, JPEG)!");
        }

        return res;
    }

    public static ImagePattern fromScreen() throws IOException {
        ImagePattern res = new ImagePattern();

        res.handle = res.api.allocateImageFromScreen();

        if(res.handle == Pointer.NULL) {
            throw new IOException("Screenshot could not be made.");
        }

        return res;
    }

    public void saveToFile(String path) throws IOException {
        if(!api.writeImageToFile(path, handle)) {
            throw new IOException("Image could not be saved to '" + path + "'.");
        }
    }

    @Override
    protected void finalize() {
        api.releaseImage(handle);
        handle = Pointer.NULL;
    }

    Pointer handle() {
        return handle;
    }
}
