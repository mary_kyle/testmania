package com.citrix.testing.utils.interaction;

/**
 * Created by natalie on 2/14/2015.
 */
public enum  MatchAlgorithm {
    SQDIFF_NORMED,
    CCORR_NORMED,
    CCOEFF_NORMED
}
