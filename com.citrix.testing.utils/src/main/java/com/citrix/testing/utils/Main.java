package com.citrix.testing.utils;

import com.citrix.testing.utils.interaction.*;

import java.io.IOException;

public class Main {
    static VirtualInput input = new VirtualInput();
    static PatternListener listener = new PatternListener("testcases/patterns/");

    public static void main(String[] args) throws IOException, InterruptedException {
        Browsers browser = Browsers.Firefox;

        completeCleanup();

        // start IE with G2M login page
        SysUtils.runUrl(browser, "http://global.gotomeeting.com");

        // signout if necessary
        if(listener.waitForPatterns("G2M/Login", "G2M/HelloMenu").patternName().equals("G2M/HelloMenu")) {

            leftClickPattern("G2M/HelloMenu");
            leftClickPattern("G2M/SignOut");

            // wait for login screen
            listener.waitForPatterns("G2M/Login");
        }

        // sign in
        leftClickPattern("G2M/SignIn");

        do {
            input.write("christoph.husse@citrix.com");
            input.press(VirtualKey.TAB);
            input.write("/\\Sa!!u3$@b[f[5;");

            leftClickPattern("G2M/SignIn");

        }while(listener.waitForPatterns("G2M/MeetNow", "G2M/EmptySignIn").patternName().equals("G2M/EmptySignIn"));

        // end pending meetings (if any)
        while(true) {
            listener.waitForPatterns("G2M/MeetNow");
            if (listener.withThreshold(0.8).waitForPatterns("G2M/HasUpcomingMeetings", "G2M/NoUpcomingMeetings").patternName().equals("G2M/HasUpcomingMeetings")) {
                leftClickWhilePattern("G2M/EndMeetingOnWebsite", 0.8);
            } else {
                break;
            }
        }

        leftClickPattern("G2M/MeetNow");

        browserAcceptDownload(browser, "G2M/Firefox_RunDownload", "G2M/Chrome_RunFile");

        // wait for launcher app

        // wait for G2M
        listener.waitForPatterns("G2M/InSession");

        completeCleanup();

    }

    private static void browserAcceptDownload(Browsers browser, String fireFoxPattern, String chromePattern) throws IOException {
        switch(browser) {
            case Firefox: {
                leftClickPattern("Firefox/SaveFile");
                leftClickPattern("Firefox/ArrowDown");
                leftClickPattern(fireFoxPattern);
                leftClickPattern("Windows/RunFile");
            } break;

            case IE: {
                leftClickPattern("IE/RunDownloadButton");
            } break;

            case Chrome: {
                leftClickPattern(chromePattern);
                leftClickPattern("Windows/RunFile");
            } break;
        }
    }

    private static void completeCleanup() throws IOException {
        SysUtils.killProcessesById(SysUtils.listProcessIdsByPattern("g2m"));

        SysUtils.killProcessesById(SysUtils.listProcessIdsByPattern("iexplore"));
        SysUtils.killProcessesById(SysUtils.listProcessIdsByPattern("chrome"));
        SysUtils.killProcessesById(SysUtils.listProcessIdsByPattern("firefox"));

        SysUtils.killProcessesById(SysUtils.listProcessIdsByPattern("citrixonlinelauncher"));

        SysUtils.uninstallProductsByPattern(null, null, "citrix");

        SysUtils.tryDeleteFSTree(citrixAppDataFolder());
        SysUtils.tryDeleteFSTree(SysUtils.tempFolder());
        SysUtils.tryCleanFSTree(SysUtils.userDownloadsFolder());

        SysUtils.deleteRegKeysByPattern(HKEY.CLASSES_ROOT, "", "^\\.citrix");
        SysUtils.deleteRegKeysByPattern(HKEY.CLASSES_ROOT, "", "^\\.g2m");
        SysUtils.deleteRegKeysByPattern(HKEY.CLASSES_ROOT, "", "^\\.gotomeeting");
        SysUtils.deleteRegKeysByPattern(HKEY.CURRENT_USER, "Software\\Citrix", ".*");
    }

    private static String citrixAppDataFolder() {
        return SysUtils.userAppDataFolder() + "/Citrix";
    }

    private static void leftClickPattern(String pattern) throws IOException {
        input.moveMouseTo(listener.waitForPatterns(pattern).center());
        input.leftClick();
    }

    private static void leftClickWhilePattern(String pattern) throws IOException {
        leftClickWhilePattern(pattern, 0.9);
    }

    private static void leftClickWhilePattern(String pattern, double threshold) throws IOException {
        do {
            ImageRegion region = listener.withThreshold(threshold).findPatternsOrNull(pattern);

            if (region != null) {
                input.moveMouseTo(region.center());
                input.leftClick();
            } else {
                return;
            }

            SysUtils.sleep(1000);
        } while(true);
    }
}
