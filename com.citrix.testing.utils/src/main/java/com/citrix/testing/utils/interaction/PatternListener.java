package com.citrix.testing.utils.interaction;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class PatternListener {
    private static final Logger logger = LogManager.getLogger(PatternListener.class);

    private double threshold = 0.9;
    private SharedState state;

    private static class SharedState {
        private File directory;
        private Map<String, ImagePattern> patterns = new HashMap<String, ImagePattern>();
        private ImageMatcher matcher = new ImageMatcher();
    }

    private PatternListener(PatternListener parent) {
        this.state = parent.state;
    }

    public PatternListener(String patternDirectory) {
        state = new SharedState();

        state.directory = new File(patternDirectory);

        if(!state.directory.isDirectory()) {
            throw new IllegalArgumentException("Expected a valid directory.");
        }
    }

    public PatternListener withThreshold(double threshold) {
        PatternListener res = new PatternListener(this);
        res.threshold = threshold;
        return res;
    }

    public ImageRegion waitForPatterns(String... patternFiles) throws IOException {

        while(true) {
            ImageRegion region = findPatternsOrNull(patternFiles);
            if(region != null) {
                return region;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.interrupted();

                throw new IOException("Interrupted while waiting for pattern '" + patternFiles[0] + "'.");
            }
        }
    }

    public ImageRegion findPatternsOrNull(String... patternFiles) throws IOException {
        if (patternFiles.length == 0) {
            throw new IllegalArgumentException("No patterns given.");
        }

        logger.trace("Looking for patterns on screen...");

        ImagePattern screen = ImagePattern.fromScreen();

        for(String patternFile : patternFiles) {
            File file = new File(state.directory.getAbsolutePath() + "/" + patternFile + ".png");
            String path = file.getAbsolutePath();
            try {
                path = file.getCanonicalPath();

                if (!state.patterns.containsKey(path)) {
                    state.patterns.put(path, ImagePattern.fromFile(path));
                }

                ImageRegion cfn = state.matcher.match(patternFile, state.patterns.get(path), screen, MatchAlgorithm.CCOEFF_NORMED);
                String matchInfo = String.format("CFN:%.2f", cfn.threshold());

//  Those other algorithms seem useless...
//                ImageRegion crn = state.matcher.match(patternFile, state.patterns.get(path), screen, MatchAlgorithm.CCORR_NORMED);
//                ImageRegion sqn = state.matcher.match(patternFile, state.patterns.get(path), screen, MatchAlgorithm.SQDIFF_NORMED);
//                double average = (cfn.threshold() + crn.threshold() + sqn.threshold()) / 3;
//                matchInfo = String.format("[CFN:%.2f;CRN:%.2f;SQN:%.2f] = %.2f", cfn.threshold(), crn.threshold(), sqn.threshold(), average);

                if (cfn.threshold() > threshold) {
                    logger.trace(String.format(">   Accepted pattern '%s' %s > %.2f", patternFile, matchInfo, threshold));
                    return cfn;
                } else {
                    logger.trace(String.format(">   Rejected pattern '%s' %s < %.2f", patternFile, matchInfo, threshold));
                }

            } catch (Throwable e) {
                throw new IOException("Could not load pattern '" + patternFile + "' from file '" + path + "'!");
            }
        }

        return null;
    }
}
