package com.citrix.testing.utils.interaction;

/**
 * Created by natalie on 2/14/2015.
 */
public enum VirtualKey {
    TAB,
    ENTER,
}
