package com.citrix.testing.utils;

/**
* Created by natalie on 2/15/2015.
*/
public enum HKEY {
    CLASSES_ROOT,
    CURRENT_USER,
    LOCAL_MACHINE
}
