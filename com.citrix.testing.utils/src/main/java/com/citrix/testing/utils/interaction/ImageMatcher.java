package com.citrix.testing.utils.interaction;

import com.citrix.testing.utils.NativeUtilsApi;

import java.util.ArrayList;
import java.util.List;

public class ImageMatcher {

    private NativeUtilsApi api = new NativeUtilsApi();

    public ImageRegion matchMedianOrNull(String templateName, ImagePattern template, ImagePattern image, double threshold) {
        List<ImageRegion> regions = new ArrayList<>();

        regions.add(match(templateName, template, image, MatchAlgorithm.SQDIFF_NORMED));
        regions.add(match(templateName, template, image, MatchAlgorithm.CCORR_NORMED));
        regions.add(match(templateName, template, image, MatchAlgorithm.CCOEFF_NORMED));

        regions.sort(null);

        for (ImageRegion r : regions) {
            if (r.threshold() < threshold) {
                return null;
            }
        }

        return regions.get(1);
    }

    public ImageRegion match(String templateName, ImagePattern template, ImagePattern image, MatchAlgorithm algorithm) {
        int alg = 0;

        switch(algorithm) {
            case SQDIFF_NORMED: alg = 1; break;
            case CCORR_NORMED: alg = 3; break;
            case CCOEFF_NORMED: alg = 5; break;
        }

        int[] rect = new int[4];
        double[] threshold = new double[1];
        api.matchTemplateAgainstImage(template.handle(), image.handle(), alg, threshold, rect);
        return new ImageRegion(templateName, rect[0], rect[3], rect[1], rect[2], threshold[0]);
    }

}
