#define _SCL_SECURE_NO_WARNINGS 1

#include <windows.h>
#include <stdint.h>
#include <Shlwapi.h>
#include <Msi.h>
#include <TlHelp32.h>
#include <Shlobj.h>

#include <array>
#include <codecvt>

#include <boost/xpressive/xpressive_dynamic.hpp>

extern "C" __declspec(dllexport) bool killProcesses(int32_t* processes, int32_t procCount) {
    BOOL result = true;

    for(int i = 0; i < procCount; i++) {
        HANDLE hProc = OpenProcess(PROCESS_TERMINATE, false, processes[i]);
        if((hProc == NULL) || (hProc == INVALID_HANDLE_VALUE)) {
            result = false;
        }

        result &= TerminateProcess(hProc, (UINT)-1);
        CloseHandle(hProc);
    }

    return result ? true : false;
}

bool doesMatch(
    const std::string& path, 
    const boost::xpressive::sregex& pathRegex) {
    
    boost::xpressive::sregex_iterator it(path.begin(), path.end(), pathRegex), end;

    return it != end;
}

extern "C" __declspec(dllexport) bool listProcesses(const char* pathPattern, int32_t* processes, int32_t* procCount) {
    
    if (!pathPattern) {
        return false;
    }

    try {
        int bufferSize = 0;
        if(procCount) {
            bufferSize = *procCount;
            *procCount = 0;

            if(processes) {
                for(int i = 0; (i < bufferSize); i++) {
                    processes[i] = 0;
                }
            }
        }

        boost::xpressive::sregex pathRegex = boost::xpressive::sregex::compile(pathPattern, 
            boost::xpressive::regex_constants::icase |
            boost::xpressive::regex_constants::ECMAScript);
        std::vector<int> matchingProcesses;
    
        HANDLE hProcessSnap;
        PROCESSENTRY32 pe32;
        hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

        if (hProcessSnap == INVALID_HANDLE_VALUE) {
        } else {
            pe32.dwSize = sizeof(PROCESSENTRY32);
            if (Process32First(hProcessSnap, &pe32)) { 
                std::string path = pe32.szExeFile;

                if (doesMatch(path, pathRegex)) {
                    matchingProcesses.push_back(pe32.th32ProcessID);
                } 

                while (Process32Next(hProcessSnap, &pe32)) { 
                    path = pe32.szExeFile;

                    if (doesMatch(path, pathRegex)) {
                        matchingProcesses.push_back(pe32.th32ProcessID);
                    }
                }
            }

            CloseHandle(hProcessSnap);
        }

        if(procCount) {
            *procCount = (int)matchingProcesses.size();

            if(processes) {
                int i = 0;

                for(; (i < bufferSize) && (i < (int)matchingProcesses.size()); i++) {
                    processes[i] = matchingProcesses[i];
                }
            }
        }

        return true;
    } catch(...) {
        return false; 
    }
}


extern "C" __declspec(dllexport) int32_t deleteRegKeysByPattern(int64_t hkey, const char* basePath, const char* keyNamePattern) {

    if (!basePath || !keyNamePattern) {
        return -3;
    }

    try {
        HKEY baseKey;
        boost::xpressive::sregex pathRegex = boost::xpressive::sregex::compile(keyNamePattern, 
                boost::xpressive::regex_constants::icase |
                boost::xpressive::regex_constants::ECMAScript);

        if(ERROR_SUCCESS != RegOpenKeyExA((HKEY)hkey, basePath, 0, KEY_ALL_ACCESS, &baseKey)) {
            return 0;
        }

        char currentSubkey[MAX_PATH];
        int i = 0;
        std::vector<std::string> keysToDelete;

        while(i >= 0) {
            DWORD currentSubLen = MAX_PATH - 1;

            if(RegEnumKeyExA(baseKey, i++, currentSubkey, &currentSubLen, NULL, NULL, NULL, NULL) == ERROR_SUCCESS)
            {
                if(doesMatch(currentSubkey, pathRegex)) {
                    keysToDelete.push_back(currentSubkey);
                }
            } else {
                break;
            }
        }

        bool result = true;
        for(auto& name : keysToDelete) {
            // delete key
            result &= (SHDeleteKeyA(baseKey, name.c_str()) == ERROR_SUCCESS);
        }

        RegCloseKey(baseKey);

        if(!result) {
            return -1;
        }

        return (int32_t)keysToDelete.size();

    } catch(...) {
        return -2;
    }
}

extern "C" __declspec(dllexport) int32_t uninstallProductsByPattern(
            const char* productCodeFilter_,
            const char* productNamePattern, 
            const char* publisherPattern) {

    try {
        if(!productNamePattern) {
            productNamePattern = "^$";
        }

        if(!publisherPattern) {
            publisherPattern = "^$";
        }

        int i = 0;
        char productCode[50];
        char productName[1024];
        char publisher[1024];

        boost::xpressive::sregex nameRegex = boost::xpressive::sregex::compile(productNamePattern, 
                boost::xpressive::regex_constants::icase |
                boost::xpressive::regex_constants::ECMAScript);
        boost::xpressive::sregex publishRegex = boost::xpressive::sregex::compile(publisherPattern, 
                boost::xpressive::regex_constants::icase |
                boost::xpressive::regex_constants::ECMAScript);

        std::string productCodeFilter;
        std::vector<std::string> productsToRemove;

        if(productCodeFilter_) {
            productCodeFilter = productCodeFilter_;
        }
    
        while(MsiEnumProductsA(i++, productCode) == ERROR_SUCCESS) {

            if (!productCodeFilter.empty()) {
                if(productCodeFilter == productCode) {
                    productsToRemove.push_back(productCode);
                    continue;
                }
            }

            DWORD productNameSize = sizeof(productName) / sizeof(productName[0]);
            DWORD publisherSize = sizeof(publisher) / sizeof(publisher[0]);

            if(MsiGetProductInfoA(productCode, "InstalledProductName", productName, &productNameSize) == ERROR_SUCCESS) {
                if(doesMatch(productName, nameRegex)) {
                    productsToRemove.push_back(productCode);
                    continue;
                }
            }

            if(MsiGetProductInfoA(productCode, "Publisher", publisher, &publisherSize) == ERROR_SUCCESS) {
                if(doesMatch(publisher, publishRegex)) {
                    productsToRemove.push_back(productCode);
                    continue;
                }
            }
        }

        MsiSetInternalUI(INSTALLUILEVEL_NONE, NULL);

        bool result = true;
        for(auto& productCode : productsToRemove) {
            result &= (ERROR_SUCCESS == MsiConfigureProductA(productCode.c_str(), INSTALLLEVEL_DEFAULT, INSTALLSTATE_ABSENT));
        }

        if(!result) {
            return -1;
        }

        return (int32_t)productsToRemove.size();

    } catch (...) {
        return -2;
    }
}

extern "C" __declspec(dllexport) const char* getKnownFolderPath(int32_t folderId) {

    PWSTR path = nullptr;

    try {
        static const int FOLDER_COUNT = 1;
        static const std::array<const KNOWNFOLDERID*, FOLDER_COUNT> folderIds = {
            &FOLDERID_Downloads,
        };

        static std::array<std::string, FOLDER_COUNT> folderPaths;
        
        typedef std::codecvt_utf8<wchar_t> convert_type;
        std::wstring_convert<convert_type, wchar_t> converter;
    
        if(S_OK != SHGetKnownFolderPath(
                        *folderIds.at(folderId),
                        0,
                        NULL,
                        &path)) {
            CoTaskMemFree(path);
            return nullptr;
        }

        folderPaths.at(folderId) = converter.to_bytes(path);

        CoTaskMemFree(path);

        return folderPaths.at(folderId).c_str();
    } catch(...) {

        CoTaskMemFree(path);

        return nullptr;
    }
}