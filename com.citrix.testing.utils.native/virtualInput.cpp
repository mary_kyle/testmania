#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <windows.h>

extern "C" __declspec(dllexport) bool moveMouseTo(int32_t x, int32_t y, int32_t steps, int32_t totalMillis) {
	POINT pos = {};
	GetCursorPos(&pos);

	float cx = (float)pos.x;
	float cy = (float)pos.y;
    bool cmpX =  (cx < x);
    bool cmpY =  (cy < y);

	const float stepX = abs((x - cx) / steps);
	const float stepY = abs((y - cy) / steps);
	const DWORD stepSleep = totalMillis / steps;
    const bool cmpFirstX = cmpX;
    const bool cmpFirstY = cmpY;

	for (; ; ) {
        cmpX =  (cx < x);
        cmpY =  (cy < y);

		if ((cmpX != cmpFirstX) && (cmpY != cmpFirstY))
			break;

        if (cmpX == cmpFirstX) {
            cx += (cmpX ? 1 : -1) * stepX;
        }

		if (cmpY == cmpFirstY) {
            cy += (cmpY ? 1 : -1) * stepY;
        }

		INPUT input = {};
		input.type = INPUT_MOUSE;
		input.mi.dx = (LONG)(cx * (65536.0f / GetSystemMetrics(SM_CXSCREEN)));
		input.mi.dy = (LONG)(cy * (65536.0f / GetSystemMetrics(SM_CYSCREEN)));
		input.mi.dwFlags = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE;

		if(!SendInput(1, &input, sizeof(input)))
			SetCursorPos((int)cx, (int)cy);

		Sleep(stepSleep);
	}

    INPUT input = {};
	input.type = INPUT_MOUSE;
	input.mi.dx = (LONG)(x * (65536.0f / GetSystemMetrics(SM_CXSCREEN)));
	input.mi.dy = (LONG)(y * (65536.0f / GetSystemMetrics(SM_CYSCREEN)));
	input.mi.dwFlags = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE;

    if(!SendInput(1, &input, sizeof(input)))
        SetCursorPos(x, y);

	return true;
}

extern "C" __declspec(dllexport) bool clickMouseButton(int32_t button) {
	(void)button;

	INPUT input = {};
	input.type = INPUT_MOUSE;
	input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
	SendInput(1, &input, sizeof(input));

	Sleep(500);

	input.type = INPUT_MOUSE;
	input.mi.dwFlags = MOUSEEVENTF_LEFTUP;
	SendInput(1, &input, sizeof(input));

    Sleep(500);

	return true;
}

extern "C" __declspec(dllexport) bool writeChar(int32_t c) {

	INPUT input = {};

    input.type = INPUT_KEYBOARD;
    input.ki.wVk = 0;
    input.ki.wScan = (WORD)c;
    input.ki.dwFlags = KEYEVENTF_UNICODE;
    input.ki.time = 0;
    input.ki.dwExtraInfo = GetMessageExtraInfo();

    SendInput(1, &input, sizeof(input));
    Sleep(50);

    input.type = INPUT_KEYBOARD;
    input.ki.wVk = 0;
    input.ki.wScan = (WORD)c;
    input.ki.dwFlags = KEYEVENTF_UNICODE | KEYEVENTF_KEYUP;
    input.ki.time = 0;
    input.ki.dwExtraInfo = GetMessageExtraInfo();

    SendInput(1, &input, sizeof(input));
    Sleep(50);

	return true;
}

extern "C" __declspec(dllexport) bool pressVirtualKey(int32_t virtualKeyCode) {

	INPUT input = {};
	
	input.type = INPUT_KEYBOARD;
	input.ki.wScan = (WORD)MapVirtualKey(virtualKeyCode, MAPVK_VK_TO_VSC);
	input.ki.time = 0;
	input.ki.dwExtraInfo = 0;
	input.ki.wVk = (WORD)virtualKeyCode;
	input.ki.dwFlags = 0;

	SendInput(1, &input, sizeof(INPUT));
    Sleep(50);

	input.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &input, sizeof(INPUT));
    Sleep(50);

	return true;
}