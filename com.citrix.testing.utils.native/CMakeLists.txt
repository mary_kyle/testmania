cmake_minimum_required(VERSION 2.8)

project (com.citrix.testing.utils.native)

set(OPENCV3_ROOT "" CACHE PATH "Root directory to your OpenCV 3.0 installation.")
set(BOOST_ROOT "" CACHE PATH "Root directory to your Boost root directory.")

set(CMAKE_CONFIGURATION_TYPES Release)
set(CMAKE_BUILD_TYPE Release)

IF (CMAKE_SIZEOF_VOID_P EQUAL 4)
    set(OCVLIB "${OPENCV3_ROOT}\\build\\x86\\vc12\\staticlib\\")
    set(ARCH 32)
ELSE()
    set(OCVLIB "${OPENCV3_ROOT}\\build\\x64\\vc12\\staticlib\\")
    set(ARCH 64)
ENDIF()

if(MSVC_VERSION EQUAL 1800)
else()
	message(FATAL_ERROR "Only Visual Studio 2013 is supported at the moment.")
endif()

macro(postbuild_copy_file src dst)
    get_filename_component(src2 "${src}" REALPATH)
    get_filename_component(dst2 "${dst}" REALPATH)

    add_custom_command(TARGET com.citrix.testing.utils.native POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy ARGS "${src}" "${dst2}")
endmacro(postbuild_copy_file)

macro(postbuild_copy_library targetDir)
    file(MAKE_DIRECTORY "${targetDir}")
    IF(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
        postbuild_copy_file("$<TARGET_FILE:com.citrix.testing.utils.native>" "${targetDir}/com.citrix.testing.utils.native${ARCH}.dll")
    ELSE()
        postbuild_copy_file("$<TARGET_FILE:com.citrix.testing.utils.native>" "${targetDir}/com.citrix.testing.utils.native${ARCH}.so")
    ENDIF()
endmacro(postbuild_copy_library)

include_directories(
	${CMAKE_CURRENT_SOURCE_DIR}
	"${OPENCV3_ROOT}\\build\\include\\"
	"${BOOST_ROOT}"
)

set(CMAKE_CXX_FLAGS_RELEASE "/MP /EHa /MT /WX /W4 /MP /MT /Ox /Ob2 /Oi /Ot /GL /D NDEBUG /GR " CACHE STRING "" FORCE)

add_library(
	com.citrix.testing.utils.native 
	SHARED 
	matchTemplateAgainstImage.cpp
	virtualInput.cpp
	sysUtils.cpp
)

target_link_libraries(
	com.citrix.testing.utils.native 
	"Comctl32.lib"
	"Shlwapi.lib"
	"Msi.lib"

	"${OCVLIB}libwebp.lib"
	"${OCVLIB}libtiff.lib"
	"${OCVLIB}libpng.lib"
	"${OCVLIB}libjasper.lib"
	"${OCVLIB}libjpeg.lib"
	"${OCVLIB}ippicvmt.lib"
	"${OCVLIB}zlib.lib"
	"${OCVLIB}opencv_core300.lib"
	"${OCVLIB}IlmImf.lib"
	"${OCVLIB}opencv_imgcodecs300.lib"
	"${OCVLIB}opencv_highgui300.lib"
	"${OCVLIB}opencv_imgproc300.lib"
)

postbuild_copy_library("${CMAKE_CURRENT_SOURCE_DIR}/../com.citrix.testing.utils/src/main/resources/")