#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"
#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <windows.h>

using namespace std;
using namespace cv;


#pragma pack(1)
struct MatchResultRect {
	int32_t left, top, bottom, right;
};
#pragma pack()

extern "C" __declspec(dllexport) cv::Mat* allocateImageFromScreen() {
	try {
		HDC hwindowDC, hwindowCompatibleDC;
		HWND hwnd = GetDesktopWindow();
		int height, width;
		HBITMAP hbwindow;
		Mat src;
		BITMAPINFOHEADER  bi;

		hwindowDC = GetDC(hwnd);
		hwindowCompatibleDC = CreateCompatibleDC(hwindowDC);

		RECT windowsize;    // get the height and width of the screen
		GetClientRect(hwnd, &windowsize);

		height = windowsize.bottom;
		width = windowsize.right;

		src.create(height, width, CV_8UC4);

		// create a bitmap
		hbwindow = CreateCompatibleBitmap(hwindowDC, width, height);
		bi.biSize = sizeof(BITMAPINFOHEADER);
		bi.biWidth = width;
		bi.biHeight = -height;  //this is the line that makes it draw upside down or not
		bi.biPlanes = 1;
		bi.biBitCount = 32;
		bi.biCompression = BI_RGB;
		bi.biSizeImage = 0;
		bi.biXPelsPerMeter = 0;
		bi.biYPelsPerMeter = 0;
		bi.biClrUsed = 0;
		bi.biClrImportant = 0;

		// use the previously created device context with the bitmap
		SelectObject(hwindowCompatibleDC, hbwindow);
		// copy from the window device context to the bitmap device context
		BitBlt(hwindowCompatibleDC, 0, 0, width, height, hwindowDC, 0, 0, SRCCOPY);
		GetDIBits(hwindowCompatibleDC, hbwindow, 0, height, src.data, (BITMAPINFO *)&bi, DIB_RGB_COLORS);

		DeleteObject(hbwindow); 
		DeleteDC(hwindowCompatibleDC); 
		ReleaseDC(hwnd, hwindowDC);

		Mat* res = new Mat();

		if ((res->channels() != 4) || (res->type() != CV_8UC4)) {
			src.convertTo(*res, CV_8UC4);
		}
		else {
			src.assignTo(*res);
		}

		if ((res->channels() != 4) || (res->type() != CV_8UC4)) {
			delete res;
			return nullptr;
		}

		return res;
	}
	catch (...) {
		return nullptr;
	}
}

extern "C" __declspec(dllexport) bool writeImageToFile(const char* path, cv::Mat* image) {
	try {
		if (!image || !path)
			return false;

		return imwrite(path, *image);
	}
	catch (...) {
		return false;
	}
}

extern "C" __declspec(dllexport) cv::Mat* allocateImageFromFile(const char* path) {
	try {
		Mat* res = new Mat();
		Mat img = imread(path, IMREAD_COLOR);

		if ((img.channels() != 4) || (img.type() != CV_8UC4)) {
			cvtColor(img, *res, CV_RGB2RGBA, 4);
		}
		else {
			img.assignTo(*res);
		}
		
		
		if ((res->channels() != 4) || (res->type() != CV_8UC4)) {
			delete res;
			return nullptr;
		}

		return res;
	}
	catch (...) {
		return nullptr;
	}
}

extern "C" __declspec(dllexport) void releaseImage(cv::Mat* img) {
	try {
		if (img) {
			delete img;
		}
	}
	catch (...) {
	}
}

extern "C" __declspec(dllexport) bool matchTemplateAgainstImage(
	cv::Mat* templateImage,
	cv::Mat* image,
	int32_t matchMethod,
	double* outThreshold,
	MatchResultRect* outResultRect) {

	try {
		if (outThreshold) {
			*outThreshold = 0;
		}

		if (outResultRect) {
			MatchResultRect empty = {};
			*outResultRect = empty;
		}

		/// Create the result matrix
		int resultCols = image->cols - templateImage->cols + 1;
		int resultRows = image->rows - templateImage->rows + 1;

		if ((resultCols < 0) || (resultRows < 0)) {
			return false;
		}

		cv::Mat result;
		result.create(resultCols, resultRows, CV_32FC1);

		/// Do the Matching and Normalize
		matchTemplate(*image, *templateImage, result, matchMethod);
		//normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());

		/// Localizing the best match with minMaxLoc
		double minVal; double maxVal; Point minLoc; Point maxLoc;
		Point matchLoc;

		minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());

		/// For SQDIFF and SQDIFF_NORMED, the best matches are lower values. For all the other methods, the higher the better
		if (matchMethod == TM_SQDIFF || matchMethod == TM_SQDIFF_NORMED)
		{
			if (outThreshold) {
				*outThreshold = 1 - minVal;
			}

			matchLoc = minLoc;
		}
		else
		{
			if (outThreshold) {
				*outThreshold = maxVal;
			}

			matchLoc = maxLoc;
		}

		if (outResultRect) {
			outResultRect->left = matchLoc.x;
			outResultRect->top = matchLoc.y;
			outResultRect->right = matchLoc.x + templateImage->cols;
			outResultRect->bottom = matchLoc.y + templateImage->rows;
		}

		return true;
	}
	catch (...) {
		return false;
	}
}